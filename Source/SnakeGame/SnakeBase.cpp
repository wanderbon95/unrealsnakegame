// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"
#include "Food.h"
#include "KillBlock.h"
#include "FloorBlock.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	LastMovementDirection = EMovementDirection::UP;
	MovementSpeed = 0.5;
	SpeedStep = -0.05;
	WidthMap = 30;
	HeightMap = 30;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);

	GenerateLevel();
	GenerateFood();

	AddSnakeElement(1);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++) {
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 1);
		FTransform NewTransform(GetActorLocation() - NewLocation);
		ASnakeElementBase* SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);

		int32 IndexElement = SnakeElements.Add(SnakeElement);

		SnakeElement->SnakeOwner = this;

		if (IndexElement == 0) {
			SnakeElement->SetFirstElementType();	
		}

		SnakeElement->SetHidden(false);
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector;
	float MovementSize = ElementSize;

	switch (LastMovementDirection) {
		case EMovementDirection::UP: {
			MovementVector.X += MovementSize;
			break;
		}
		case EMovementDirection::DOWN: {
			MovementVector.X -= MovementSize;
			break;
		}
		case EMovementDirection::LEFT: {
			MovementVector.Y -= MovementSize;
			break;
		}
		case EMovementDirection::RIGHT: {
			MovementVector.Y += MovementSize;
			break;
		}
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];

		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement)) {
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);

		if (InteractableInterface) {
			int32 ElementIndex;
			SnakeElements.Find(OverlappedElement, ElementIndex);

			InteractableInterface->Interact(this, ElementIndex == 0);
		}
	}
}

void ASnakeBase::GenerateFood()
{
	int xMin = (-WidthMap / 2) * (ElementSize - 10) + (ElementSize - 10) * 3;
	int xMax = (WidthMap / 2) * (ElementSize - 10) - (ElementSize - 10) * 3;

	int yMin = (-HeightMap / 2) * (ElementSize - 10) + (ElementSize - 10) * 3;
	int yMax = (HeightMap / 2) * (ElementSize - 10) - (ElementSize - 10) * 3;

	int x = FMath::Floor(FMath::FRand() * (xMax - xMin + 1)) + xMin;
	int y = FMath::Floor(FMath::FRand() * (yMax - yMin + 1)) + yMin;

	bool isNotEmpty = false;

	for (int i = 0; i < SnakeElements.Num(); i++) {
		ASnakeElementBase* currentSnakeElemnt = SnakeElements[i];

		FVector currentLocation = currentSnakeElemnt->GetActorLocation();
		float currentX = currentLocation.X;
		float currentY = currentLocation.Y;

		if (x >= currentX - (ElementSize - 10) && x <= currentX + (ElementSize - 10)) {
			isNotEmpty = true;
			break;
		}

		if (y >= currentY - (ElementSize - 10) && y <= currentY + (ElementSize - 10)) {
			isNotEmpty = true;
			break;
		}

		isNotEmpty = false;
	}

	if (isNotEmpty) {
		return GenerateFood();
	}

	FTransform NewTransform;
	NewTransform.SetLocation(FVector(x, y, 1));

	GetWorld()->SpawnActor<AFood>(FoodClass, NewTransform);
}

void ASnakeBase::GenerateLevel() {
	for (int i = -WidthMap / 2; i < WidthMap / 2; i++) {
		for (int j = -HeightMap / 2; j < HeightMap / 2; j++) {
			FTransform NewFloorTransform;
			NewFloorTransform.SetLocation(FVector(i * (ElementSize - 10) , j * (ElementSize - 10), -(ElementSize - 11)));

			GetWorld()->SpawnActor<AFloorBlock>(FLoorBlockClass, NewFloorTransform);

			if (i == -WidthMap / 2 || i == WidthMap / 2 - 1 || j == - HeightMap / 2 || j == HeightMap / 2 - 1) {
				FTransform NewKillTransform;
				NewKillTransform.SetLocation(FVector(i * (ElementSize - 10), j * (ElementSize - 10), 0));

				GetWorld()->SpawnActor<AKillBlock>(KillBlockClass, NewKillTransform);
			}
		}
	}
}

void ASnakeBase::Restart() {
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}