// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SnakeElementBase.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class AFood;
class AKillBlock;
class AFloorBlock;

UENUM()
enum class EMovementDirection {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	void GenerateLevel();
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AKillBlock> KillBlockClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFloorBlock> FLoorBlockClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(VisibleAnywhere)
		float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
		float SpeedStep;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
		EMovementDirection LastMovementDirection;

	UPROPERTY(EditDefaultsOnly)
		int WidthMap;

	UPROPERTY(EditDefaultsOnly)
		int HeightMap;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElement(int ElementsNum = 1);

	void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	UFUNCTION()
		void GenerateFood();

	UFUNCTION()
		void Restart();
};
